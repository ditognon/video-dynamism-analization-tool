import vector
import frame
import subprocess


from vector import Vector
from frame import Frame


#Returns a list containing all motion vectors for a given video
def load_vectors(path_to_video): 
    #Collection of all frames
    video = []

    print("Extracting motion vectors...")
    #call mpegflow with path_to_video to extract motion vectors
    with open("output.txt", "w") as outfile:
        subprocess.run(["../mpegflow/mpegflow", "--raw", path_to_video], stdout=outfile)

    #Loading vectors
    print("Loading motion vectors...")
    try:
        vector_file = open("output.txt") #file created by mpegflow

        for row in vector_file:
            if(row.startswith("#")): #start of a new frame
                working_frame = Frame(row.split(" ")[2].split("=")[1])
                video.append(working_frame)
            else:
                temp = row.split("\t")
                #print(temp)
                working_vector = Vector(int(temp[0]), int(temp[1]), int(temp[3][:-1]), int(temp[2]))
                working_frame.add_vector(working_vector)
        vector_file.close()
    except FileNotFoundError:
        print("Invalid path to file!")

    return video
