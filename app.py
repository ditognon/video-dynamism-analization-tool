import tkinter as tk
import threading
import queue
import time
import hvfunc as hvf
import os


from tkinter import ttk
from tkinter import *
from tkinter import filedialog
from os import path
from PIL import ImageTk, Image
import calculator
from calculator import Calculator, ASSVCalculator, UVLCalculator, TSCalculator, PNNVCalculator

class App(tk.Tk):

    def __init__(self, calculator):
        tk.Tk.__init__(self)
        self.calculator = calculator

        self.history_list = []
        self.help_list = [
        "Welcome to help!",
        "This program evaluates dynamism of videos using different algorithms.",
        "To Choose an algorithm use the menu \"Algorithm\".",
        "You can enter a path in the text box or choose a video file by pressing \"Choose File\".", 
        "Once you have choosen a file press \"Verify\" to check the path.",
        "Then you can evaluate your video by pressing \"Evaluate\".",
        "If you want to check what you previously evaluated just press on the \"History\" button!"
        ]

        self.queue_send = queue.Queue()
        self.queue_receive = queue.Queue()
        self.msg_queue = queue.Queue()

        self.cmp_ready1 = 0
        self.cmp_ready2 = 0

        self.title("Video Dynamism Analization Tool")

        self.resizable(False, False)

        self.main_menu = tk.Menu(self)
        self.config(menu=self.main_menu)

        self.algorithm_menu = tk.Menu(self.main_menu)
        self.main_menu.add_cascade(label="Algorithm", menu=self.algorithm_menu)
        self.algorithm_menu.add_command(label="Average Square Sum of Vectors(ASSV)", command=lambda: self.set_calculator(ASSVCalculator()))
        self.algorithm_menu.add_command(label="Unified Vector Length(UVL)", command=lambda: self.set_calculator(UVLCalculator()))
        self.algorithm_menu.add_command(label="Total Sum(TS)", command=lambda: self.set_calculator(TSCalculator()))
        self.algorithm_menu.add_command(label="Percentage of Not Null Vectors(PNNV)", command=lambda: self.set_calculator(PNNVCalculator()))
        self.main_menu.add_command(label="History", command=self.openHistoryWindow)
        self.main_menu.add_command(label="Help", command=self.openHelpWindow)

        self.text_entry1 = StringVar()
        self.text_entry2 = StringVar()

        self.text_entry1.trace_add("write", lambda var, indx, mode: self.handlePath(self.entry1, self.verify_signal1, self.eval_btn1, 1))
        self.text_entry2.trace_add("write", lambda var, indx, mode: self.handlePath(self.entry2, self.verify_signal2, self.eval_btn2, 0))

        # Definitions
        self.entry1 = tk.Entry(self, width=50, textvariable=self.text_entry1)
        self.entry2 = tk.Entry(self, width=50, textvariable=self.text_entry2)

        self.choose_file_btn_1 = tk.Button(self, text="Choose File", command = lambda: self.choose_file_function(self.entry1))
        self.choose_file_btn_2 = tk.Button(self, text="Choose File", command = lambda: self.choose_file_function(self.entry2))

        self.verify_signal1 = tk.Label(self)
        self.verify_signal2 = tk.Label(self)
        
        self.good_img = ImageTk.PhotoImage(Image.open("/home/dino/ZR/images/good.png"))
        self.bad_img = ImageTk.PhotoImage(Image.open("/home/dino/ZR/images/bad.png"))

        self.eval_btn1 = tk.Button(self, text="Evaluate", state="disabled")
        self.eval_btn1.config(command= lambda: self.spawn_evaluate_thread(1))
        self.eval_btn2 = tk.Button(self, text="Evaluate", state="disabled")
        self.eval_btn2.config(command= lambda: self.spawn_evaluate_thread(2))
        self.res_label1 = tk.Label(self, text="R:~~~")
        self.res_label2 = tk.Label(self, text="R:~~~")

        self.cmp_btn = tk.Button(self, text="Compare", state="disabled")
        self.cmp_btn.config(command=self.spawn_compare_thread)

        self.res_label = tk.Label(self, text="V1 > V2: ")

        self.status_label = tk.Label(self, text="")

        # Placement
        self.entry1.grid(row=0, column=0, padx=5)
        self.entry2.grid(row=1, column=0, padx=5, pady=5)

        self.choose_file_btn_1.grid(row=0, column=1)
        self.choose_file_btn_2.grid(row=1, column=1)

        self.verify_signal1.grid(row=0, column=2, padx=5)
        self.verify_signal2.grid(row=1, column=2, padx=5)

        self.eval_btn1.grid(row=0, column=3, padx=5, pady=10)
        self.eval_btn2.grid(row=1, column=3, padx=5, pady=10)

        self.res_label1.grid(row=0, column=4)
        self.res_label2.grid(row=1, column=4)

        self.status_label.grid(row=2, column=0)
        self.cmp_btn.grid(row=2, column=3)
        self.res_label.grid(row=2, column=4)

    def set_calculator(self, new_calculator):
        self.calculator = new_calculator

    def lockEvaluateButtons(self, button):
        button.config("state=disabled")
        self.cmp_btn.config("state=disabled")


    def openHistoryWindow(self):
        self.history = tk.Toplevel(self)
    
        self.history.title("History")
        self.history.geometry("800x300")
    
        for h in self.history_list:
            tk.Label(self.history, text=h).pack()

    def openHelpWindow(self):
        self.help = tk.Toplevel(self)
    
        self.help.title("Help")
        self.help.geometry("600x180")

        for h in self.help_list:
            tk.Label(self.help, text=h).pack()


    def choose_file_function(self, entry):
        filename = filedialog.askopenfilename(initialdir = "/home/dino/ZR/videos",title = "Select file")
        entry.delete(0,"end")
        entry.insert(0, filename)

    def spawn_evaluate_thread(self, id):
        self.eval_btn1.config(state="disabled")
        self.eval_btn2.config(state="disabled")
        self.cmp_btn.config(state="disabled")
        if id == 1:
            self.queue_send.put(self.entry1.get())
        elif id == 2:
            print(self.entry2.get())
            self.queue_send.put(self.entry2.get())
        else:
            print("Error!")
            self.quit()
        self.active_thread = EvaluateThread(self.calculator, self.queue_receive, self.queue_send, self.msg_queue)
        self.active_thread.start()
        self.periodiccall(id)

    def spawn_compare_thread(self):
        self.eval_btn1.config(state="disabled")
        self.eval_btn2.config(state="disabled")
        self.cmp_btn.config(state="disabled")
        self.queue_send.put(self.entry1.get())
        self.queue_send.put(self.entry2.get())
        self.active_thread = CompareThread(self.calculator, self.queue_receive, self.queue_send, self.msg_queue)
        self.active_thread.start()
        self.periodiccall(self.res_label)

    def periodiccall(self, id):
        self.checkqueue(id)
        if self.active_thread.is_alive():
            self.after(100, lambda: self.periodiccall(id))


    def checkqueue(self, id):

        while self.msg_queue.qsize():
            try:
                msg = self.msg_queue.get(0)
                self.status_label.config(text=msg)
            except Queue.Empty:
                pass

        while self.queue_receive.qsize():
            try:
                res = self.queue_receive.get(0)
                if id == 1:
                    self.res_label1.config(text="R:" + str(res))
                    self.history_list.append(self.calculator.name() + " " + self.entry1.get() + " ===> " + str(res))
                elif id == 2:
                    self.res_label2.config(text="R:" + str(res))
                    self.history_list.append(self.calculator.name() + " " + self.entry2.get() + " ===> " + str(res))
                else:
                    self.history_list.append(self.calculator.name() + " " + self.entry1.get() + " ===> " + str(res))
                    self.history_list.append(self.calculator.name() + " " + self.entry2.get() + " ===> " + str(res))
                    self.res_label.config(text="R:" + str(res))
            except Queue.Empty:
                pass

    # Returnes True if path is a path to video file
    def verify_path_to_video(path_to_file):
        extensions = (".mp4", ".avi") # Recognised video file extensions
        if path.exists(path_to_file):
            for e in extensions:
                if path_to_file.endswith(e):
                    return True

        return False
    
    def handlePath(self, entry, signal, button, cmpid):
        if App.verify_path_to_video(entry.get()):
            signal.config(image=self.good_img)
            button.config(state="active")
            if cmpid == 1:
                self.cmp_ready1 = 1
            else:
                self.cmp_ready2 = 1
        else:
            signal.config(image=self.bad_img)
            button.config(state="disabled")
            if cmpid == 1:
                self.cmp_ready1 = 0
            else:
                self.cmp_ready2 = 0

        if self.cmp_ready1 == 1 and self.cmp_ready2 == 1:
            self.cmp_btn.config(state="active")
        else:
            self.cmp_btn.config(state="disabled")
        
class EvaluateThread(threading.Thread):
    semload = threading.Semaphore()
    semcalc = threading.Semaphore()
    semsend = threading.Semaphore()
    def __init__(self, calculator, queue_send, queue_receive, msg_queue):
        threading.Thread.__init__(self)
        self.calculator = calculator
        self.queue_send = queue_send
        self.queue_receive = queue_receive
        self.msg_queue = msg_queue
    def run(self):
        self.msg_queue.put("Extracting and loading motion vectors..")
        EvaluateThread.semload.acquire()
        video = hvf.load_vectors(self.queue_receive.get())
        EvaluateThread.semload.release()

        self.msg_queue.put("Calculating..")
        print("Calculating...")
        EvaluateThread.semcalc.acquire()
        res = self.calculator.calculate_movement(video)
        EvaluateThread.semcalc.release()

        print(res)
        EvaluateThread.semsend.acquire()
        self.msg_queue.put("Done!")
        self.queue_send.put(res)
        EvaluateThread.semsend.release()

class CompareThread(threading.Thread):
    def __init__(self, calculator, queue_send, queue_receive, msg_queue):
        threading.Thread.__init__(self)
        self.calculator = calculator
        self.queue_send = queue_send
        self.queue_receive = queue_receive
        self.msg_queue = msg_queue

    def run(self):
        self.msg_queue.put("Extracting and loading motion vectors for video1..")
        video1 = hvf.load_vectors(self.queue_receive.get())
        self.msg_queue.put("Extracting and loading motion vectors for video2..")
        video2 = hvf.load_vectors(self.queue_receive.get())

        self.msg_queue.put("Calculating..")
        print("Calculating...")
        res = self.calculator.calculate_movement(video1) > self.calculator.calculate_movement(video2)

        print("Result: " + str(res))
        self.msg_queue.put("Done!")
        self.queue_send.put(res)

if __name__ == "__main__":
    app = App(ASSVCalculator())
    app.mainloop()