class Vector:
    def __init__(self, posX, posY, x, y):
        self.posX = posX
        self.posY = posY
        self.x = x
        self.y = y

    def __str__(self):
        return self.posX + " " + self.posY + " " + self.x + " " + self.y