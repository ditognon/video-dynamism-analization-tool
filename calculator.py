from abc import ABC, abstractmethod
import math

class Calculator(ABC):
    @abstractmethod
    def calculate_movement(video):
        pass

    @abstractmethod
    def name(self):
        pass
# Length complete - Two videos of the same dynamism and different length are rated the same
# Resolution complete - Two videos of the same dinamicity and different resolution are rated the same

# Average Square Sum of Vectors algorithm
# Length complete
# ~Resolution complete
class ASSVCalculator(Calculator):
    def calculate_movement(self, video):
        sum = 0
        video_length = len(video)

        for tmp_frame in video:
            for tmp_vector in tmp_frame.vector_list:
                sum += ((tmp_vector.x**2 + tmp_vector.y**2)/video_length)
        return sum

    def name(self):
        return "ASSV"


# Unified Vector Length algorithm
# Condenses all vectors into one vector and returns it's length
# Length complete
# ~Resolution complete
class UVLCalculator(Calculator):
    def calculate_movement(self, video):
        abs_sum_x = 0
        abs_sum_y = 0
        n_of_vectors = 0

        for frame in video:
            for vector in frame.vector_list:
                abs_sum_x += abs(vector.x)
                abs_sum_y += abs(vector.y)
                n_of_vectors += 1

        unified_x = abs_sum_x / n_of_vectors
        unified_y = abs_sum_y / n_of_vectors

        return math.sqrt(unified_x**2 + unified_y**2)

    def name(self):
        return "UVL"

# Total Sum Algorithm
# ~Length complete
# ~Resolution complete
class TSCalculator(Calculator):
    def calculate_movement(self, video):
        sum = 0

        for frame in video:
            for vector in frame.vector_list:
                sum += vector.x**2 + vector.y**2

        return sum

    def name(self):
        return "TS"

# Percentage of Not Null Vectors
# Length complete
# ~Resolution complete
# Problem sa odabirom frame-a za broj vektora - potencijalno dijeljenje sa nulom
class PNNVCalculator(Calculator):
    def calculate_movement(self, video):
        n_of_not_null_vectors = 0

        for frame in video:
            for vector in frame.vector_list:
                if vector.x != 0:
                    n_of_not_null_vectors += 1
                elif vector.y != 0:
                    n_of_not_null_vectors +=1
        
        return ((n_of_not_null_vectors / len(video))/len(video[1].vector_list)) * 100

    def name(self):
        return "PNNV"
