class Frame:
    def __init__(self, index):
        self.vector_list = []
        self.index = index

    def __str__(self):
        return str(self.vector_list)

    def add_vector(self, v):
        self.vector_list.append(v)